#!/usr/bin/env sh
NEW_USER=$1
PASSWORD=$2

if [ -z "$NEW_USER" -o -z "$PASSWORD" ]; then
  echo "Syntax: ./create-user.sh <username> <password>"
  exit 1
fi

useradd $NEW_USER -G sudo -m

echo -e "$PASSWORD\n$PASSWORD" | passwd $NEW_USER
