#!/usr/bin/env sh
# POSIX only! - not run in bash

# Supports
# - MacOS 10.14+
# - Fedora 28+
# - CentOS 7+
# - Ubuntu (most recent versions)
# - Debian (most recent versions?)

if [ `whoami` = "root" -a "$CI" != "true" ]; then
  echo "Do not run this script as root"
  exit 1
fi

# Remove pyenv from the path as we must use the system python to run Ansible
export PATH=`echo $PATH | tr ':' '\n' | sed '/pyenv/d' | tr '\n' ':' | sed 's/:$//'`

# Deactivate any virtual environments
if [ ! -z "$VIRTUAL_ENV" ]; then
  deactivate
fi

if [ `uname` = "Darwin" ]; then
  if [ -z `command -v brew` ]; then
    echo "Installing Homebrew..."
    # CI=1 makes Homebrew install silently
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
    # Fix some zsh permissions errors after installing homebrew
    chmod -R go-w "$(brew --prefix)/share"
    profile=$HOME/.zprofile
    if [[ $(basename $SHELL) == "bash" ]]; then
      profile=$HOME/.bash_profile
    fi
    echo 'eval "$(/usr/local/bin/brew shellenv)"' >> $profile
    eval "$(/usr/local/bin/brew shellenv)"
  fi
  if [ -z `command -v pyenv` ]; then
    echo "Installing pyenv"
    brew install pyenv
    pyenv install 3.10
    echo 'export PYENV_ROOT="$HOME/.pyenv' >> $profile
    echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH' >> $profile
    echo 'eval "$(pyenv init -)"' >> $profile
    eval "$(/usr/local/bin/brew shellenv)"
    pyenv global 3.10
  fi
  if [ -z `command -v ansible` ]; then
    echo "Installing Ansible..."
    brew install ansible python3
  fi
else
  if [ -z `command -v pip3` ]; then
    echo "Installing Python3 'pip3'"
    if [ `command -v apt` ]; then
      sudo apt update
      sudo apt install python3-pip git -y
    elif [ `command -v dnf` ]; then
      sudo dnf install python3-pip git -y
    elif [ `command -v yum` ]; then
      sudo yum install -y python3-pip git
     else
      echo "`pip3` must be installed at the system level (PyEnv and VirtualEnv cannot be used). No package manager was available. Install pip3 in the system path before continuing."
      exit 6
    fi
  fi

  MIN_ANSIBLE_MAJOR_VERSION=2
  MIN_ANSIBLE_MINOR_VERSION=9
  if [ `command -v ansible` ]; then
    ANSIBLE_VERSION=$(ansible --version | cut -d' ' -f2)
    ANSIBLE_VERSION_MAJOR=$(echo $ANSIBLE_VERSION | cut -d'.' -f1)
    ANSIBLE_VERSION_MINOR=$(echo $ANSIBLE_VERSION | cut -d'.' -f2)
    # anything less than 2.9 will not have all features we need
    if [ $ANSIBLE_VERSION_MAJOR -lt $MIN_ANSIBLE_MAJOR_VERSION -o $ANSIBLE_VERSION_MINOR -lt $MIN_ANSIBLE_MINOR_VERSION ]; then
      echo "Upgrading Ansible..."
      sudo pip3 install --upgrade ansible
    fi
  else
    echo "Installing Ansible..."
    sudo pip3 install ansible
  fi
fi

TSG_HOME=${TSG_HOME:-$HOME/tsg}

if [ ! -d "$TSG_HOME/setup" ]; then
  mkdir -p $TSG_HOME
  # cloning with https so no auth is needed as we may not have setup SSH keys yet
  git clone https://bitbucket.org/stronach/setup.git $TSG_HOME/setup
fi


if [ ! -h "$HOME/setup.sh" ]; then
  rm "$HOME/setup.sh"
  # link the setup.sh file to $HOME/setup.sh so that it is in
  # the same place it would be when it's downloaded when following the README instructions
  ln -s $TSG_HOME/setup/setup.sh $HOME/setup.sh
fi

#TODO: temporarily until we merge to master
git -C $TSG_HOME/setup checkout ${CIRCLE_BRANCH:-master}
git -C $TSG_HOME/setup pull --ff-only origin ${CIRCLE_BRANCH:-master}

exec $TSG_HOME/setup/run-ansible.sh $@

