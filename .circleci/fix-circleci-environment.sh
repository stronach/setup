# Uninstall chruby (as it causes issues with the 1/ST repository because of `.ruby-version`)

[ `command -v brew` ] && brew uninstall chruby ; \
[ -f ~/.bashrc ] && cat ~/.bashrc | sed '/^.*chruby.*$/d' > ~/.bashnew && mv ~/.bashnew ~/.bashrc ; \
[ -f ~/.zshrc ] && cat ~/.zshrc | sed '/^.*chruby.*$/d' > ~/.bashnew && mv ~/.bashnew ~/.zshrc ; \
[ -f ~/.zsh_profile ] && cat ~/.zsh_profile | sed '/^.*chruby.*$/d' > ~/.bashnew && mv ~/.bashnew ~/.zsh_profile ; \
[ -f ~/.bash_profile ] && cat ~/.bash_profile | sed '/^.*chruby.*$/d' > ~/.bashnew && mv ~/.bashnew ~/.bash_profile ; \
true

# Remove circleci's installed go 1.15 from the path

# result=$(echo "$PATH" | sed "s/\/usr\/local\/go\/bin://g"); \
# echo "export PATH=/home/circleci/.local/bin:$result" >> $BASH_ENV

# Fix Heroku gpg key (circleci image issue)

[ "$(uname -s)" = "Linux" ] && curl https://cli-assets.heroku.com/apt/release.key | sudo apt-key add - || true
