- become: no
  when: ansible_distribution == 'MacOSX'
  block:

    - stat:
        path: "{{ docker.osx.installed_location }}"
      changed_when: False
      register: dockerinstallation

    - homebrew_cask:
        name: docker
        state: present # docker updates itself
      when: not dockerinstallation.stat.exists

    - name: Start Docker
      shell: open -a Docker
      register: dockerstart
      


- when: ansible_distribution == 'CentOS'
  become: yes
  block:

    - name: Remove old Docker packages (CentOS)
      dnf:
        name:
          - docker
          - docker-client
          - docker-client-latest
          - docker-latest
          - docker-latest-logrotate
          - docker-logrotate
          - docker-engine
        state: absent

    - name: Add repository (CentOS)
      yum_repository:
        name: docker-ce
        description: Docker Yum Repository
        baseurl: https://download.docker.com/linux/{{ ansible_distribution | lower }}/$releasever/$basearch/stable
        gpgcheck: yes
        gpgkey: https://download.docker.com/linux/{{ ansible_distribution | lower }}/gpg

    - name: Install Docker (CentOS)
      dnf:
        state: present
        update_cache: true
        name:
          - docker-ce
          - docker-ce-cli
          - containerd.io

- when: ansible_distribution == 'Fedora'
  become: yes
  block:

    - name: Install Docker (Fedora)
      dnf:
        state: present
        update_cache: true
        name: podman

    - name: Symbolic link docker -> podman
      ansible.builtin.file:
        src: /usr/bin/podman
        dest: "{{ ansible_user_dir }}/.local/bin/docker"
        state: link

    - blockinfile:
        path: "{{ developer_bash_profile }}"
        create: yes
        marker: "# {mark} ANSIBLE MANAGED - PODMAN DOCKER COMPAT"
        state: present
        block: |
          export BUILDAH_FORMAT=docker

- when: ansible_distribution == 'Ubuntu' or ansible_distribution == 'Debian'
  become: true
  block:

    - name: Remove old Docker packages
      apt:
        name:
          - docker
          - docker-engine
          - docker.io
          - containerd
          - runc
        state: absent

    - name: Add package dependencies
      apt:
        name:
          - apt-transport-https
          - ca-certificates
          - curl
          - gnupg-agent
          - software-properties-common
          - gpg
        state: present

    - name: Add repository key
      when: ansible_distribution == 'Ubuntu'
      apt_key:
        state: present
        url: https://download.docker.com/linux/{{ ansible_distribution | lower }}/gpg

    - name: Get Ubuntu Release name
      command: lsb_release -cs
      changed_when: False
      register: lbs_release_cs

    - name: Add repository
      apt_repository:
        repo: "deb [arch=amd64] https://download.docker.com/linux/{{ ansible_distribution | lower }} {{ lbs_release_cs.stdout.rstrip('\n') }} stable"
        update_cache: true
        state: present

    - name: Install Docker
      apt:
        state: present
        update_cache: true
        name:
          - docker-ce
          - docker-ce-cli
          - containerd.io

- name: Add User to Docker group
  become: yes
  register: add_docker_group
  user:
    name: "{{ ansible_user_id }}"
    append: true
    groups: docker
  when: ansible_distribution == 'Fedora' or ansible_distribution == 'CentOS' or ansible_distribution == 'Ubuntu' or ansible_distribution == 'Debian'

- name: Start Docker service (CentOS)
  become: yes
  systemd:
    enabled: yes
    state: started
    name: docker
  when: ansible_distribution == 'CentOS'

- name: Set FirewallBackend=iptables
  become: yes
  lineinfile:
    state: present
    dest: /etc/firewalld/firewalld.conf
    line: FirewallBackend=iptables
    regexp: ^FirewallBackend=
  notify:
    - restart firewalld
    - restart docker
  when: ansible_distribution == 'Fedora'
