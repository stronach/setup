1. Update OS to current. If on windows, install DLL dependencies
2. Install brew (Mac) or chocolatey (Windows)
3. Install git, gcloud
4. Verify git connection to bitbucket: 
   > git config -l | grep user.name
5. Verify gcloud connection to the google cluster
   > gcloud iam service-accounts list | grep user.name
