#!/usr/bin/env sh
# POSIX only! - not run in bash

if [ -z `command -v realpath` ]; then
  realpath() {
    OURPWD=$PWD
    cd "$(dirname "$1")"
    LINK=$(readlink "$(basename "$1")")
    while [ "$LINK" ]; do
      cd "$(dirname "$LINK")"
      LINK=$(readlink "$(basename "$1")")
    done
    REALPATH="$PWD/$(basename "$1")"
    cd "$OURPWD"
    echo "$REALPATH"
  }
fi

# current directory must be the location of the script itself
DIR=$(dirname `realpath "$0"`)
cd $DIR

SECRET_BASH_PROFILE=""
if [ `uname` = "Darwin" -a `basename $SHELL` = 'bash' -a -f ~/.bash_profile ]; then
  SECRET_BASH_PROFILE="user_profile_path: '~/.bash_profile'"
fi

# Create ansible secrets
if [ ! -f "secrets.yaml" ]; then
  if [ -z "$GIT_NAME" ]; then
    read -p "What is your full name: " GIT_NAME
  fi
  if [ -z "$GIT_EMAIL" ]; then
    read -p "What is the email address you use for BitBucket: " GIT_EMAIL
  fi
  if [ -z "$SERVICE_ACCOUNT_NAME" ]; then
    read -p "What would you like your service account in Google Cloud developer project to be called? Defaults to '${USER}-sa'. If you already have one, enter it here: " SERVICE_ACCOUNT_NAME
    SERVICE_ACCOUNT_NAME=${SERVICE_ACCOUNT_NAME:-${USER}-sa}
  fi
  cat << EOF >> secrets.yaml
---
full_name: $GIT_NAME
email: $GIT_EMAIL
service_account_name: $SERVICE_ACCOUNT_NAME
$SECRET_BASH_PROFILE
EOF
fi

# Initialize ssh keys if not present
if [ ! -f "$HOME/.ssh/id_rsa" -a "$CI" != "true" ]; then
  SSH_PASS=""
  VERIFY_PASS=""
  while [ ${#SSH_PASS} -lt 8 -o "$VERIFY_PASS" != "$SSH_PASS" ]
  do
    if [ ! -z "$SSH_PASS" -a ${#SSH_PASS} -lt 8 ]; then
      echo "Pass phrase must be at least 8 characters"
    elif [ ! -z "$VERIFY_PASS" ]; then
      echo "Pass phrases did not match"
    fi
    read -p "Enter a new SSH pass phrase of at least 8 characters: " SSH_PASS

    if [ ${#SSH_PASS} -gt 7 ]; then
    read -p "Re-enter the pass phrase: " VERIFY_PASS
    fi
  done

  echo "Generating new SSH key in $HOME/.ssh/id_rsa"

  ssh-keygen -q -t rsa -N "$SSH_PASS" -f ~/.ssh/id_rsa

  if [ `uname` = "Darwin" ]; then
    ssh-add -K
  else
    ssh-add "$HOME/.ssh/id_rsa"
  fi
  new_ssh_key=true
fi

# ignored if already exists
mkdir -p $HOME/.ssh

# add BitBucket's fingerprint to known_hosts
if [ ! -f "$HOME/.ssh/known_hosts" -o -z "$(ssh-keygen -F bitbucket.org)" ]; then
  curl -sS https://bitbucket.org/site/ssh >> "$HOME/.ssh/known_hosts"
fi

# setup SSH config
if [ ! -f "$HOME/.ssh/config" -a "$SKIP_CREATE_SSH" != "1" ]; then
echo "IgnoreUnknown UseKeychain

Host *
  UseKeychain yes
  AddKeysToAgent yes
  IdentityFile ~/.ssh/id_rsa
" \
      > "$HOME/.ssh/config"
  chmod 700 $HOME/.ssh/config
fi

# Check if we have SSH access to the 1st monorepo
if [ ! -z "$new_ssh_key" -o -z "$(git ls-remote -q git@bitbucket.org:stronach/1st.git master)" ]; then
  if [ "$CI" = "true" ]; then
    echo "Unable to access 1/ST monorepo BitBucket via SSH"
    exit 1
  fi

  cat << EOF
                         ,d
                         88
             ,adPPYba, MM88MMM ,adPPYba,  8b,dPPYba,
             I8[    ""   88   a8"     "8a 88P'    "8a
              '"Y8ba,    88   8b       d8 88       d8
             aa    ]8I   88,  "8a,   ,a8" 88b,   ,a8"
             '"YbbdP"'   "Y888 '"YbbdP"'  88'YbbdP"'
                                          88
                                          88

        ___           ___           ___           ___
       /\\__\\         /\\  \\         /\\__\\         /\\  \\
      /:/  /        /::\\  \\       /::|  |       /::\\  \\
     /:/__/        /:/\\:\\  \\     /:|:|  |      /:/\\:\\  \\
    /::\\  \\ ___   /::\\~\\:\\  \\   /:/|:|__|__   /::\\~\\:\\  \\
   /:/\\:\\  /\\__\\ /:/\\:\\ \\:\\__\\ /:/ |::::\\__\\ /:/\\:\\ \\:\\__\\
   \\/__\\:\\/:/  / \\/_|::\\/:/  / \\/__/~~/:/  / \\/__\\:\\/:/  /
        \\::/  /     |:|::/  /        /:/  /       \\::/  /
        /:/  /      |:|\\/__/        /:/  /        /:/  /
       /:/  /       |:|  |         /:/  /        /:/  /
       \\/__/         \\|__|         \\/__/         \\/__/

EOF
    echo "----- Add the following key to your BitBucket SSH authorized keys ------"
    echo "------ See https://bitbucket.org/account/user/YOUR_USER/ssh-keys/ -----------"
    echo ""
    cat "$HOME/.ssh/id_rsa.pub"
    echo ""
    echo "------------------------------------------------------------------------"
    read -p "Press ENTER after you have added the key to continue..." ENTER
fi

# Remove pyenv from the path as we must use the system python to run Ansible
export PATH="`echo $PATH | tr ':' '\n' | sed '/pyenv/d' | tr '\n' ':' | sed 's/:$//'`"

# Deactivate any .venv virtual environments
if [ ! -z "$VIRTUAL_ENV" ]; then
  deactivate
fi

if [ "$REQS" != "0" ]; then
  ansible-galaxy collection install -r requirements.yml || ansible-galaxy collection install -r requirements.yml --force
fi
if [ "$CI" != "true" ]; then
  ASK_BECOME_PASS=--ask-become-pass
fi
echo "In the 'BECOME password:' prompt below, enter your user password. This is used for sudo."
ansible-playbook $ASK_BECOME_PASS developer-workstation.yaml $@
