# Developer Workstation Mac/Linux Setup

### Supported Operating Systems

- Windows 10 (separate script, see: https://bitbucket.org/stronach/1st/src/develop/developer-setup/)
- Mac OS X (recommended Catalina 10.15)
- Ubuntu 20
- Linux Mint 20  (additional ubuntu flavors available with minimal effort)
- CentOS 7, and 8 (partial support)
- Fedora 32 (partial support)

## Prerequirements

1. MUST have `sudo` or `Administrator` access on your PC
1. MUST have a Google Cloud sign-in and be a member of the "1/ST Developers" group in the Google Cloud Platform "Betmix" Organization.
1. MUST have an account in BitBucket with access to the Stronach team (The setup script creates an SSH key that you will need to add to your profile in BitBucket).
1. MUST have a unique local user name (i.e. if you are using a VM with a "default" user, or you were assigned a laptop with a default user like "qa", then make your own account.)
1. (Mac OS Only) - MUST install XCode from the App Store and open it once to accept the license agreement.
1. SHOULD update your operating system to the latest version

## Usage

After performing all the prerequisites above, execute this in a terminal:

```bash
cd $HOME && \
    curl -Ss https://bitbucket.org/stronach/setup/raw/master/setup.sh > setup.sh || \
    wget https://bitbucket.org/stronach/setup/raw/master/setup.sh -O setup.sh && \
    chmod +x setup.sh && \
    ./setup.sh
```

By default, only the minimum system requirements to build the 1/ST Monorepo are installed. See [Additional Components](#iinux-script-optionalcomponents) for instructions on specifying more components to install

> You will be prompted to enter a new ssh passphrase multiple times and your current user password.

Then:

* (Mac OS Only) Open Docker.app and give it permission to install docker binaries. This will put `docker`, `kubectl`, etc in your path.
* Open a new terminal (or `source ~/developer_bash_profile.sh` in an existing terminal)

> Note: when running the `full`, `client`, `backend`, `gcloud` or `fastlane` tags, if you do not already have a GCloud service account provisioned, one will be created for you. A member of the EJO team must grant your service account access rights. Message the #ejo channel in Slack with your `service_account_name` (from `~tsg/setup/secrets.yml`). Re-run setup after your account is granted access. 

## Troubleshooting

If you experience an error or to run the updated setup, the script can usually be re-run with:
```bash
~/setup.sh
```

Once it has completed successfully, the script is designed to be rerun using the below commands (i.e. `~/setup.sh` and & `$HOME\setup.ps1` )

Each time you would like to run the install Linux/OS X setup, you must specify the tags you would like to upgrade/install as well. `~/setup.sh -t client`

If yarn fails complaining about the PROVE_NPM_TOKEN run this
```bash
export PROVE_NPM_TOKEN=$(gcloud secrets versions access latest --secret="PROVE_NPM_TOKEN" --project="1009418669766")
```

## Optional Components

By passing a `-t tag` parameter to either `setup.sh` or `run-ansible.sh`,
different software can be installed. Multiple tags can be specified by separating them with commas. (i.e. `-t vscode,fastlane`)

Passing `-t full` will install all packages supported by this setup.

```bash
cd $HOME && \
    curl -Ss https://bitbucket.org/stronach/setup/raw/master/setup.sh > setup.sh || \
    wget https://bitbucket.org/stronach/setup/raw/master/setup.sh -O setup.sh && \
    chmod +x setup.sh && \
    ./setup.sh -t full
```

### developer

The default tag `-t developer` (implied if you do not specify any other tags). 

### client

If you are an engineer that works on web development for iOS client devices and are on a Mac OS system, you should run the `-t client` tag

This implies the `developer`, `gcloud` and `fastlane` tags

### backend

If you are an engineer that works on middleware and backend systems, you should run the `-t backend` tag

This implies the `developer`, `docker`, and `gcloud` tags

### docker

Passing `-t docker` will install Docker Desktop for your OS

Note: on Linux OS (not on Mac), you will need to completely restart your computer after Docker is installed initially before your user account can access the `docker` cli

### vscode

Passing `-t vscode` will install Visual Studio Code for your OS

### gcloud

Passing `-t gcloud` will install and configure the `gcloud`, `helm`, and `kubectl` command line tools
  * `gcloud` login requires your system browser be set to Chrome on Mac OS which happen automatically and will you will need to confirm when prompted while running Ansible
  * this will also create a GCloud service account if you do not already have one - message the #EJO channel in slack with your service account to have it granted access to the development project

### fastlane

Passing `-t fastlane` will install all requirements to run the `fastlane` CLI and build Cordova for iOS applications (MacOS only). This also implies the `gcloud` tag as it is required to build fastlane apps for iOS.

## Scripts

### `setup.ps1` - (Windows only) 

Complete system setup script that installs all packages mentioned above in "Provides" and some of "Optional" (specifically `gcloud`)

### `setup.sh` - (Linux/Mac OS) 

Bootstraps your system with any minimum requirements and checks out the 'setup' repository into `$HOME/tsg/setup` before running ansible. Arguments are passed through to `ansible-playbook`

### `run-ansible.sh` (Linux/Mac OS) - 

Runs the ansible playbook for a developer workstation (assuming ansible is installed). Arguments are passed through to `ansible-playbook`.

## Potential Problems

If fastlane says `Unable to locate Xcode` and you do have xcode install you may need to switch to the *correct* version of XCode with something like: `sudo xcode-select -switch /Applications/Xcode.app/Contents/Developer`

If running `setup.sh` give an error like: `ERROR! Cannot meet requirement community.general:1.1.0 as it is already installed at version '1.2.0'. Use --force to overwrite` then install the package it asks for with: `ansible-galaxy collection install community.general:1.1.0 --force`

## Provides

- Homebrew (Mac OS only) (i.e. `brew`)
- Chocolatey (Windows only) (i.e. `choco`)
- `wget` and `curl` (if not already installed)
- Git installation and configuration
- Clone and setup of Stronach Enterprise BitBucket git repositories ( Source located at `~/tsg` )
- Keychain (on OSs that don't support it natively) to save SSH key passwords
- Golang (i.e. `go`) {latest system version}
- NodeJS (i.e. `node`) {version specified in .nvmrc of 1/ST monorepo in $HOME/tsg/1st/.nvmrc}
- `nvm` - NodeJS version manager (to install other NodeJS versions)
- Python 3 (i.e. `python`, `python3`) {latest version}
- JQ JSON cli library (i.e. `jq`)
- Dotnet Core SDK (i.e. `dotnet`) {latest version}
- Protobuf `protoc` runtime and headers and `prototool` (and related tools)
- Google APIs protobuf libraries
- gRPC Gateway libraries and runtime
- Windows 10 SDK (Windows only)
- Docker Desktop (Windows required, MacOS optional)
- Bash/Zsh Completion helpers for all above supported utilities

### Optional 
- Google Cloud SDK installation/updating and configuration (i.e. `gcloud`) - via the `gcloud`, `client`, `backend` or `full` tags
  - Also installs Google Chrome on Mac OS only and makes Chrome default browser (as this is required to log in to Gcloud CLI)
  - You may change your default browser back after setup. It is only required during initial login.
  - You will need to confirm the selection when prompted
- Install Visual Studio Code (does not upgrade if already installed) - via the `vscode` or `full` tags
- Install Docker Desktop (does not upgrade if already installed) (MacOS) - via the `docker`, `backend` or `full` tags
- `fastlane` and Ruby support - via the `client`, `fastlane`, or `full` tags
